// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAyebfFTzlJTAVkOqelqHIhubbcp-UgV9E",
    authDomain: "eduwebgl.firebaseapp.com",
    databaseURL: "https://eduwebgl.firebaseio.com",
    projectId: "eduwebgl",
    storageBucket: "eduwebgl.appspot.com",
    messagingSenderId: "359887702954",
    appId: "1:359887702954:web:e0817e8ab20ea9cfc3f9d3"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
