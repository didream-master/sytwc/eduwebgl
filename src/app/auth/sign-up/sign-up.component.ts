import { Component } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/auth.service';
import { Router } from '@angular/router';
import { AUTHENTICATED_PATH } from 'src/app/app.constants';

const AuthErrorMessages = {
    'auth/email-already-in-use': 'El correo electrónico está en uso',
    'auth/weak-password': 'La contraseña debe tener al menos 6 caracteres',
    'auth/invalid-email': 'Correo electrónico inválido'
}

@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.component.html'
})
export class SignUpComponent {

    public form: FormGroup;
    public showPassword = false;

    public formErrorMessage: string;

    constructor(
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private router: Router
    ) {

    }
    ngOnInit() {
        this.form = this.formBuilder.group({
            email: [null, [Validators.required, Validators.email]],
            password: [null, Validators.required]
        })
    }

    onSubmit() {
        this.formErrorMessage = null;
        const {email, password} = this.form.value;
        this.authService.signUp(email, password)
            .then(_ => {
                this.router.navigate(AUTHENTICATED_PATH)
            })
            .catch(err => {
                if (err.code && err.code in AuthErrorMessages) {
                    this.formErrorMessage = AuthErrorMessages[err.code];
                }
                else {
                    this.formErrorMessage = 'Ha ocurrido un error';
                }
            })
    }
}