import { Component } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/auth.service';
import { Router } from '@angular/router';
import { AUTHENTICATED_PATH } from 'src/app/app.constants';

const AuthErrorMessages = {
    'auth/user-not-found': 'El correo electrónico no está registrado',
    'auth/wrong-password': 'Contraseña incorrecta'
}
@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.component.html'
})
export class SignInComponent {
    public form: FormGroup;
    public showPassword = false;
    public formErrorMessage: string;

    constructor(
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private router: Router
    ) {

    }
    ngOnInit() {
        this.form = this.formBuilder.group({
            email: [null, [Validators.required, Validators.email]],
            password: [null, Validators.required]
        })
    }

    onSubmit() {
        this.formErrorMessage = null;

        const {email, password} = this.form.value;
        this.authService.signIn(email, password)
            .then(_ => {
                this.router.navigate(AUTHENTICATED_PATH);
            })
            .catch((err: {code: string, message: string}) => {
                if (err.code && err.code in AuthErrorMessages) {
                    this.formErrorMessage = AuthErrorMessages[err.code];
                }
                else {
                    this.formErrorMessage = 'Ha ocurrido un error';
                }
            })
    }
}