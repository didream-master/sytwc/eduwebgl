import { Pipe, PipeTransform } from "@angular/core";

const CriteriaTranslations = {
    color: {
        RED: 'rojas'
    },
    geometry: {
        SPHERE: 'esféricas',
        BOX: 'cuadradas',
        CONE: 'cónicas'
    }
}

@Pipe({
    name: 'callengeCriterias',
})
export class CallengeCriteriasPipe implements PipeTransform {

    transform(criteriasObject: {[key: string]: any}, ...args: any[]) {
        const challengeTextParts = Object.keys(criteriasObject).map(criteria => {
            return CriteriaTranslations[criteria][ criteriasObject[criteria] ] || criteriasObject[criteria]
        })

        const lastCriteriaIndex = challengeTextParts.length - 1;
        return challengeTextParts.length == 1 ? challengeTextParts[0] : challengeTextParts.slice(0, lastCriteriaIndex).join(', ').concat(` y ${ challengeTextParts[lastCriteriaIndex] }`);
    }

}