import { Component, Output, EventEmitter, Input } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ILevelData } from '../game.model';

@Component({
    selector: 'app-game-form',
    templateUrl: './game-form.component.html'
})
export class GameFormComponet {
    @Output()
    public submitOptions: EventEmitter<ILevelData> = new EventEmitter();

    @Input() levelsData: ILevelData[];

    public form: FormGroup;

    constructor(
        private formBuilder: FormBuilder
    ) {}


    ngOnInit() {
        this.form = this.formBuilder.group({
            level: [null, [ Validators.required ]]
        });
    }

    public onSubmit() {
        const { level: levelIndex } = this.form.value;
        this.submitOptions.emit(this.levelsData[levelIndex]);
    }
}