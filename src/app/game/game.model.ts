import { Mesh } from "three";
import * as THREE from 'three';

export const WAIT_TIME = 4000;
export const INTERVAL_CREATION_TIME = 1000;
export const DEFAULT_COLORS = {
    RED: 0xff0000,
    GREEN: 0x00ff00,
    BLUE: 0x0000ff
}
export const DEFAULT_SPEED = 0.1;

export interface InteractionMesh extends Mesh {
    cursor?: string;
    on?: (type: string, handler: (event: any) => void) => void;
};

export enum GeometryType {
    BOX = 'BOX',
    SPHERE = 'SPHERE',
    CONE = 'CONE'
}

export interface IGeometryBuilder {
    build: (material: THREE.MeshPhongMaterial, metadata?: {[key: string]: any}) => InteractionMesh
}

export interface ILevelData {
    id: string,
    levelName: string,
    frequency?: number,
    colors?: any;
    geometries?: GeometryType[];
    movement?: {
        speed: number | [number, number]
    },
    challenge: {
        geometry: GeometryType,
        color: string
    }
}

export const GeometryBuilder: { [k in GeometryType]: IGeometryBuilder } = {
    [GeometryType.BOX]: {
        build: (material: THREE.MeshPhongMaterial, metadata) => {
            metadata.geometry = GeometryType.BOX;
            const geometry = new THREE.BoxGeometry(3, 3, 3);
            return new THREE.Mesh( geometry, material);
        }
    },
    [GeometryType.CONE]: {
        build: (material: THREE.MeshPhongMaterial, metadata) => {
            metadata.geometry = GeometryType.CONE;
            const geometry = new THREE.ConeGeometry(1.5, 3, 32);
            return new THREE.Mesh( geometry, material);
        }
    },
    [GeometryType.SPHERE]: {
        build: (material: THREE.MeshPhongMaterial, metadata) => {
            metadata.geometry = GeometryType.SPHERE;
            const geometry = new THREE.SphereGeometry(1.5, 32, 16);
            return new THREE.Mesh(geometry, material);
        }
    }
}