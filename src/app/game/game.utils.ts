import * as THREE from 'three';
import { ILevelData, DEFAULT_COLORS, GeometryBuilder, IGeometryBuilder, DEFAULT_SPEED, InteractionMesh } from './game.model';

export function getVisibleDimensions(camera) {
    var vFOV = camera.fov * Math.PI / 180;;
    var h = 2 * Math.tan( vFOV / 2 ) * camera.position.z;
    var aspect = window.innerWidth / window.innerHeight;
    return {
        height: h,
        width: h * aspect
    }
}
export function getRandom(min, max): number {
    return Math.random() * (max - min) + min;
}

export function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

export function getRandomPosition(x, y): [number, number, number] {
    return [getRandomInt(-x, x), getRandomInt(-y, y), 0];
}


export function getMaterial(metadata: any, {colors: levelColors, challenge}: ILevelData, random: boolean = true): THREE.MeshPhongMaterial {
    const colors = levelColors || DEFAULT_COLORS;

    const colorNames = Object.keys(colors);
    const colorName = random == false && challenge.color != null ? challenge.color : colorNames[getRandomInt(0, colorNames.length)];

    metadata.color = colorName;
    return new THREE.MeshPhongMaterial( { color: colors[colorName] } );
}

export function getMesh(material: THREE.MeshPhongMaterial, metadata, {geometries: levelGeometries, challenge }: ILevelData, random: boolean = true) {
    const geometryTypes = levelGeometries || Object.keys(GeometryBuilder);
    const randomGeometryType = random == false && challenge.geometry != null ? challenge.geometry : geometryTypes[getRandomInt(0, geometryTypes.length)];

    return (GeometryBuilder[randomGeometryType] as IGeometryBuilder).build(material, metadata);
}

export function getRandomSpeedMovement(levelData: ILevelData): number {
    const speedRange = levelData.movement.speed != null ? levelData.movement.speed : DEFAULT_SPEED;

    if (Array.isArray(speedRange)) {
        const [minSpeed, maxSpeed] = speedRange;
        if (speedRange.length >= 2) {
            return getRandom(minSpeed, maxSpeed);
        }
        else {
            return minSpeed != null ? minSpeed : DEFAULT_SPEED;
        }
    }
    return speedRange as number;
}

export function setMovement(mesh: InteractionMesh, metadata, levelData: ILevelData) {
    const {x, y} = mesh.position;

    const xMove = -(x / Math.abs(x)) * getRandomSpeedMovement(levelData);
    const yMove = -(y / Math.abs(y)) * getRandomSpeedMovement(levelData);

    metadata.resolvePosition = () => {
        mesh.position.x += xMove;
        mesh.position.y += yMove;
    }
}

export function meetsCriteria(challengeCriterias, meshMetadata): boolean {
    return Object.keys(challengeCriterias).every(criteria => meshMetadata[criteria] == challengeCriterias[criteria])
}
