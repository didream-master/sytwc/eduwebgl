import { Component, ViewChild, ElementRef } from '@angular/core';
import * as THREE from 'three';
import { Interaction } from 'three.interaction';
import { AuthService } from '../core/auth.service';
import { GeometryType, INTERVAL_CREATION_TIME, WAIT_TIME, ILevelData } from './game.model';
import { getVisibleDimensions, getMaterial, getMesh, getRandomPosition, setMovement, meetsCriteria } from './game.utils';
import { Router } from '@angular/router';
import { UNAUTHENTICATED_PATH } from '../app.constants';
import { DataLevelsService } from '../core/data-levels.service';


// Obtener random material
// obtener random mesh
// calculate random position
// calculate random speed
// Añadir metadata a la geometría con la información de material, colores...

interface IGameScore {
    success: number,
    fails: number,
    totalSuccess: number,
}

@Component({
    selector: 'app-game',
    templateUrl: 'game.component.html',
    styleUrls: ['./game.component.scss']
})
export class GameComponent {
    @ViewChild('gameCanvas', {read: ElementRef, static: true}) gameCanvas: ElementRef<HTMLCanvasElement>;

    public user: firebase.User;

    public gameScore: IGameScore;
    public lastGameStore: IGameScore; 
    
    public levelsData: ILevelData[] = [];

    public levelData: ILevelData;

    private renderer = new THREE.WebGLRenderer;
    private animationFrameId: number;
    private intervalTimeId: NodeJS.Timeout;


    constructor(
        private router: Router,
        private authService: AuthService,
        private dataLevelsService: DataLevelsService,
    ) {}

    ngOnInit() {
        this.dataLevelsService.getAll()
            .subscribe(levelsData => {
                this.levelsData = levelsData;
            });
        this.user = this.authService.userData;
    }

    public onSubmitOptions(levelDataSelected: ILevelData) {
        this.levelData = levelDataSelected;
        //this.gameOptions = gameOptions;

        this.initGame();
    }

    private initGame() {
        this.gameScore = {
            fails: 0,
            success: 0,
            totalSuccess: 0
        };
        
        this.renderer = new THREE.WebGLRenderer({
            canvas: this.gameCanvas.nativeElement
        });
        const renderer = this.renderer;

        renderer.setClearColor(0x151a30);
        renderer.setSize(window.innerWidth, window.innerHeight);

        const camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 100);
        camera.position.set(0, 0, 20);

        const scene = new THREE.Scene();
        const interaction = new Interaction(renderer, scene, camera);
        const dimensions: {width: number, height: number} = getVisibleDimensions(camera);

        this.intervalTimeId = setInterval(() => {
            const meshMetadata: any = {};
            const random = this.levelData.frequency == null || Math.random() <= this.levelData.frequency;

            // get material
            const material = getMaterial(meshMetadata, this.levelData, random);

            // get mesh
            const mesh = getMesh(material, meshMetadata, this.levelData, random);

            // set random position
            const meshOriginPosition = getRandomPosition(dimensions.width / 2, dimensions.height / 2);
            mesh.position.set(...meshOriginPosition);


            // calculate movement
            if (this.levelData.movement) {
                setMovement(mesh, meshMetadata, this.levelData);
            }
            
            // set mesh metadata
            mesh.userData = meshMetadata;

            scene.add( mesh );
            mesh.cursor = 'pointer';

            if ((meetsCriteria(this.levelData.challenge, meshMetadata))) {
                this.gameScore.totalSuccess++;
            }

            const deletionTimeout = setTimeout(() => {
                scene.remove(mesh);
            }, WAIT_TIME);

            mesh.on('click', (event) => {
                clearTimeout(deletionTimeout);

                const meshMetadata = event.currentTarget.userData;

                scene.remove(mesh);
                
                if (meetsCriteria(this.levelData.challenge, meshMetadata)) {
                    this.gameScore.success++;
                }
                else {
                    this.gameScore.fails++;
                }
            })
        }, INTERVAL_CREATION_TIME);

        // luces
        const color = 0xFFFFFF;
        const intensity = 1;
        const light = new THREE.PointLight(color, intensity);
        light.position.set(5, 10, 5);
        scene.add(light);


        window.addEventListener('resize', () => {
            camera.aspect = window.innerWidth / window.innerHeight;
            camera.updateProjectionMatrix();
            renderer.setSize(window.innerWidth, window.innerHeight);
        })


        var animate = () => {
            this.animationFrameId = requestAnimationFrame( animate );

            scene.traverse( function( mesh ) {

                if ( mesh instanceof THREE.Mesh ) {
                    if (mesh.userData && mesh.userData.resolvePosition) {
                        mesh.userData.resolvePosition();
                    }
                }
            
            } );
            
            renderer.render( scene, camera );
        };

        animate();
    }

    public onLogout() {
        this.authService.signOut()
            .then(_ => {
                this.user = null;
                this.router.navigate(UNAUTHENTICATED_PATH);
            })
    }

    public onStopGame() {
        cancelAnimationFrame(this.animationFrameId);
        clearTimeout(this.intervalTimeId);
        
        this.lastGameStore = this.gameScore;
        this.levelData = null;

        this.renderer.setRenderTarget(null);
        this.renderer.state.reset();
    }
}