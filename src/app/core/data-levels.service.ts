import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ILevelData } from '../game/game.model';

@Injectable({
    providedIn: 'root'
})
export class DataLevelsService {
    private readonly path: string = 'data-levels';

    constructor(
        public firestore: AngularFirestore,
    ) {
    }

    getAll(): Observable<ILevelData[]> {
        return this.firestore.collection(this.path).get()
            .pipe(
                map(res => res.docs.map(doc => {
                    const levelData = doc.data() as ILevelData;
                    levelData.id = doc.id;
                    return levelData
                }))
            )
    }
}