import { Injectable } from '@angular/core';
import { auth } from 'firebase/app';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { User } from './user.model';

@Injectable({
    providedIn: 'root'
})

export class AuthService {
    public userData: firebase.User;

    constructor(
        public firestore: AngularFirestore,
        public fireAuth: AngularFireAuth,
    ) {}

    // Sign in with email/password
    signIn(email, password) {
        return this.fireAuth.signInWithEmailAndPassword(email, password)
            .then((result) => {
                return this.setUserData(result.user);
            })
    }

    // Sign up with email/password
    signUp(email, password) {
        return this.fireAuth.createUserWithEmailAndPassword(email, password)
            .then((result) => {
                //this.sendVerificationMail();
                return this.setUserData(result.user);
            })
    }

    /*
    // Send email verfificaiton when new user sign up
    sendVerificationMail() {
        return this.fireAuth.currentUser
            .then((user: firebase.User) => user.sendEmailVerification());
    }

    // Reset Forggot password
    
    forgotPassword(passwordResetEmail) {
        return this.fireAuth.sendPasswordResetEmail(passwordResetEmail)
            .then(() => {
                console.error('Password reset email sent, check your inbox.');
            }).catch((error) => {
                console.error(error)
            })
    }
    */

    get isLoggedIn(): boolean {
        return this.userData != null;
    }

    // Sign in with Google
    googleAuth() {
        return this.authLogin(new auth.GoogleAuthProvider());
    }

    // Auth logic to run auth providers
    authLogin(provider) {
        return this.fireAuth.signInWithPopup(provider)
            .then((result) => {
                this.setUserData(result.user);
            }).catch((error) => {
                console.error(error)
            })
    }

    /* Setting up user data when sign in with username/password, 
    sign up with username/password and sign in with social auth  
    provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
    setUserData(user) {
        const userRef: AngularFirestoreDocument<any> = this.firestore.doc(`users/${user.uid}`);
        const userData: User = {
            uid: user.uid,
            email: user.email,
            displayName: user.displayName,
            photoURL: user.photoURL
        }

        userRef.set(userData, {merge: true});
        this.userData = user;
        return userData;
    }

    // Sign out 
    signOut() {
        return this.fireAuth.signOut()
            .then(() => {
                this.userData = null;
            })
    }
}