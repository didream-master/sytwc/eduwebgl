import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GameComponent } from './game/game.component';
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule, AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { environment } from 'src/environments/environment';
import { SignInComponent } from './auth/sign-in/sign-in.component';
import { SignUpComponent } from './auth/sign-up/sign-up.component';
import { CallengeCriteriasPipe } from './game/challenge-criterias.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbInputModule, NbButtonModule, NbIconModule, NbFormFieldModule, NbAlertModule, NbSelectModule, NbUserModule, NbPopoverModule, NbListModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { RouterModule } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthService } from './core/auth.service';
import { GameFormComponet } from './game/game-form/game-form.component';

export function accountInitializer(fireAuth: AngularFireAuth, authService: AuthService) {
  return () => new Promise(resolve => {
    fireAuth.authState.pipe(first())
      .subscribe(user => {
        authService.userData = user;
        resolve(null);
      })
  })
}

@NgModule({
  declarations: [
    AppComponent,
    GameComponent,
    GameFormComponet,
    CallengeCriteriasPipe,
    SignInComponent,
    SignUpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    RouterModule,

    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,

    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'dark' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbIconModule,
    NbInputModule,
    NbButtonModule,
    NbFormFieldModule,
    NbAlertModule,
    NbSelectModule,
    NbUserModule,
    NbPopoverModule,
    NbListModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: accountInitializer,
      deps: [AngularFireAuth, AuthService],
      multi: true
  },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
