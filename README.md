# Eduwebgl

Demo disponible en [https://eduwebgl.didream.vercel.app/](https://eduwebgl.didream.vercel.app/).

El presente proyecto consiste en un juego de selección de geometrías que cumplan un determinado criterío, que la geometría sea un cubo y de color verde, por ejemplo. Está implementado con Angular 9, Three.js y firebase para la persistencia de datos. Además utiliza la authenticación de firebase.

El proyecto permite configurar los niveles de juego. Los datos del nivel deben cumpliar la siguiente interfaz.

```typescript
GeometryType {
    BOX = 'BOX',
    SPHERE = 'SPHERE',
    CONE = 'CONE'
}

ILevelData {
    id: string,
    levelName: string,
    frequency?: number,
    colors?: any;
    geometries?: GeometryType[];
    movement?: {
        speed: number | [number, number]
    },
    challenge: {
        geometry: GeometryType,
        color: string
    }
}
```

por ejemplo:

```typescript
{
    colors: {
        RED: 0xff0000,
        BLUE: 0x0000ff,
        DADA: 0xDADADA
    },
    movement: {
        speed: [0.01, 0.2]
    },
    frequency: 0.7,
    challenge: {
        geometry: GeometryType.BOX,
        color: 'RED'
    }
}
```
En el campo `challenge` se especificarán los criteríos de selección del nivel.

## Despliegue del proyecto en local
**Requisitos previos**
* Node ^12.14.0

Una vez clonado el proyecto ejecutar:

```bash
npm install
```

El proyecto hace uso de firestore de firebase para la persistencia de datos. En caso se quiera utilizar un proyecto propio de firebase es necesario actualizar la configuración en `src/environments/environment.ts` y `src/environments/environment.prod.ts`.

Posteriormente, ejecutar:

```bash
npm start
```

## Generar los ficheros estáticos para despliegue en producción
Para generar los ficheros estáticos ejecutar:
```bash
npm run build:prod
```